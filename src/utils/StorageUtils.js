let firebase = require('./FirebaseConfig');
let mime = require('mime-types');
const uuidv1 = require('uuid/v1');//this for unique id generation

class StorageUtils {

	static uploadPotScreenshot(filePath, potId) {
		//console.log("uploading file " + filePath);
		//let remotePath = `/screenshots/${potId}/${new Date().getTime()}.jpg`;
		let remotePath = `/screenshots/${potId}/pot_camera.jpg`;
		return this.upload(filePath, remotePath);
	}

	static upload(filePath, remoteFile) {
		//console.log("uploading remoteFile " + remoteFile);
		let bucket = firebase.storage().bucket();
		//console.log("uploading bucket " + bucket);
		const fileMime = mime.lookup(filePath);
		//console.log("uploading fileMime " + fileMime);
		let uuid = uuidv1();
		return new Promise((resolve, reject) => {
			console.log("StorageUtils uploading...");
			let promise = bucket.upload(filePath, {
				destination: remoteFile,
				uploadType: "media",
				metadata: {
					contentType: fileMime,
					metadata: {
						firebaseStorageDownloadTokens: uuid
					}
				}
			})
			console.log(promise)
			promise.then((data) => {
				let file = data[0];
				let downloadUrl = "https://firebasestorage.googleapis.com/v0/b/" + bucket.name + "/o/" + encodeURIComponent(file.name) + "?alt=media&token=" + uuid;
				console.log("StorageUtils upload success downloadUrl: " + downloadUrl);
				resolve(downloadUrl);
			}).catch(reason => {
				console.log("StorageUtils upload ERROR " + JSON.stringify(reason));
				reject(reason);
			});
		})
	}
}

module.exports = StorageUtils;
