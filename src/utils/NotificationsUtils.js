const firebase = require('./FirebaseConfig');
const moment = require('moment');
const {log} = require('./Log');

const SCHEDULE_MAX_TEMPERATURE = 'SCHEDULE_MAX_TEMPERATURE';
const SCHEDULE_MIN_TEMPERATURE = 'SCHEDULE_MIN_TEMPERATURE';
const SCHEDULE_PHOTO_REQUEST = 'SCHEDULE_PHOTO_REQUEST';

const actionsArray = [SCHEDULE_MAX_TEMPERATURE, SCHEDULE_MIN_TEMPERATURE, SCHEDULE_PHOTO_REQUEST];

const localNotificationsCache = {};

const notificationIsAllowed = (startUnix) => {
	const startDate = moment.unix(startUnix);
	const currentDate = moment();
	let pastMinutes = Math.trunc(moment.duration(currentDate.diff(startDate)).asMinutes());

	if (pastMinutes < 30) {
		return false;
	}

	return true;
};

const sendMessage = async (action, userId, title, body) => {
	const USER_ROUTE = `/users/${userId}`;

	if (action) {
		if (actionsArray.find(a => a === action)) {
			if (localNotificationsCache[action]) {
				if (!notificationIsAllowed(localNotificationsCache[action])) {
					return false;
				}
			}
			const user = await firebase.database().ref(USER_ROUTE).once('value').then(snapshot => snapshot.val());
			if (user && user.fcmTokens) {
				if (user.lastPotNotification && user.lastPotNotification[action]) {
					localNotificationsCache[action] = user.lastPotNotification[action];
					if (!notificationIsAllowed(user.lastPotNotification[action])) {
						return false;
					}
				}
				await firebase.messaging().sendToDevice(user.fcmTokens, {
					notification: {
						title,
						body
					}
				});
				await firebase.database().ref(USER_ROUTE + '/lastPotNotification/' + action).set(moment().unix());
				localNotificationsCache[action] = moment().unix();
				log('The notification was sent.');
				return true;
			} else {
				throw new Error(`User doesn't have message tokens`);
			}
		} else {
			throw new Error(`Invalid notification action ${action} 2`);
		}
	} else {
		throw new Error(`Undefined notification action ${action} 1`);
	}
}

module.exports = {
	sendMessage,
	SCHEDULE_MAX_TEMPERATURE,
	SCHEDULE_MIN_TEMPERATURE,
	SCHEDULE_PHOTO_REQUEST
};