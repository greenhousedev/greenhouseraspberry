const pinsDefinitions = {
	LIGHT_SENSOR_SDA: 2,
	LIGHT_SENSOR_SCL: 3,
	TEMPERATURE_AND_HUMIDITY: 4,
	LIGHT_RELAY: 17,
	PUMP_RELAY: 22,
	FAN_RELAY: 27,
};

function verifyPins() {
	const pinsKeys = Object.keys(pinsDefinitions);
	pinsKeys.forEach((key) => {
		const unique = pinsKeys.filter(key2 => pinsDefinitions[key2] === pinsDefinitions[key]);
		if (unique && unique.length > 1) {
			unique.forEach(key => console.log(`${key}: ${pinsDefinitions[key]}`));
			throw new Error(`Duplicate pin number usage [${unique.join(' ')}] ${pinsDefinitions[unique[0]]}`);
		}
	})
}

verifyPins();
module.exports = pinsDefinitions;
