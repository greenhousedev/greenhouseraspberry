const {spawn} = require('child_process');

/**
 * @return {Promise<any>}
 */
function scan() {
	return new Promise((resolve, reject) => {
		const getAllProcess = spawn('iwlist', ['wlan0', 'scan']);

		const grep = spawn('grep', ['ESSID']);

		getAllProcess.stdout.pipe(grep.stdin);

		grep.stdout.on('data', (data) => {
			console.log(`Number of files ${data}`);
			console.log(data.toString().split("ESSID:"))
			let results = data.toString().split("ESSID:")
			results = results
				.map(ssid => ssid.trim().replace('"', '').replace('"', '')).filter(ssid => ssid);
			console.log(results);
		});
	})
}

module.exports = {
	scan
};
// scan();

