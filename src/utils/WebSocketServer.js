const http = require('http').createServer();
const io = require('socket.io')(http);
const dns = require('dns');

http.listen(3000, () => {
	console.log('Websocket server listen on *:3000');
});
io.on('connection', (socket) => {
	checkInternet();
});

function checkInternet() {
	dns.resolve('www.google.com', (err) => {
		if (err) {
			io.emit('no_internet');
		} else {
			io.emit('internet_connected');
		}
	});
}

setInterval(() => {
	checkInternet();
}, 60 * 1000);

module.exports = io;