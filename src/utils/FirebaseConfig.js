let firebase = require('firebase-admin');
let serviceAccount = require(__dirname + '/../../nohaegrowing-firebase-adminsdk-yng4q-4524f6a984');

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://nohaegrowing.firebaseio.com",
    storageBucket: "gs://nohaegrowing.appspot.com"
});

module.exports = firebase;
