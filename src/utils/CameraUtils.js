const PiCamera = require('pi-camera');
const photoPath = `${ __dirname }/../../photos/potScreenshot.jpg`
const myCamera = new PiCamera({
    mode: 'photo',
    output: photoPath,
    width: 640,
    height: 480,
    nopreview: true,
    rotation: 180
});

class CameraUtils {

    static takePhoto() {
        return new Promise((resolve, reject) => {
            console.log("CameraUtils taking photo...");
            myCamera.snap()
                .then((result) => {
                    // Your picture was captured
                    console.log("CameraUtils photo captured "+photoPath);
                    resolve(photoPath);
                })
                .catch((error) => {
                    // Handle your error
                    console.log("CameraUtils::takePhoto error="+error);
                    reject(error);
                });
        })
    }
}

module.exports = CameraUtils;
