const moment = require('moment');
const io = require('./WebSocketServer');
let lastLogs = [];

io.on('connection', (socket) => {
	console.log('a user connected');
	lastLogs.forEach(log => {
		io.emit('log', JSON.stringify(log));
	});
	socket.on('log_updates', () => {
		lastLogs.forEach(log => {
			io.emit('log', JSON.stringify(log));
		});
	})
});


module.exports = {
	log: (...messages) => {
		const date = moment();
		let logObject = {
			date: date.toDate(),
			messages
		};
		if (lastLogs.length < 5) {
			lastLogs.push(logObject)
		} else {
			lastLogs.shift();
			lastLogs.push(logObject);
		}
		io.emit('log', JSON.stringify(logObject));
		console.log(`[${date.format('MM/DD hh:mm:ss')}]`, ...messages);
	}
};