const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(path.join(__dirname, '/../../reactApp')));
app.get('*.js', (req, res, next) => {
	req.url = req.url + '.gz';
	res.set('Content-Encoding', 'gzip');
	next();
});

app.get('*', (req, res) => {
	return res.sendFile(path.join(__dirname, '/../../reactApp/index.html'));
});

const start = () => {
	return new Promise((resolve, reject) => {
		app.listen(8080, async err => {
			if (err) {
				console.log(err);
				reject();
			} else {
				console.log('Web server started on port 8000');
				resolve();
			}
		});
	})
};

module.exports = {
	startWebServer: start
};

