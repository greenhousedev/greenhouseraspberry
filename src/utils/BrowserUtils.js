const {exec} = require('shelljs');

const startBrowser = ()=>{
	exec('DISPLAY=:0 chromium-browser --noerrdialogs --kiosk http://localhost:8080 --incognito --disable-translate', function(code, stdout, stderr) {
		console.log('Exit code:', code);
		console.log('Program output:', stdout);
		console.log('Program stderr:', stderr);
	});
};

module.exports = {
	startBrowser
};