let ScheduleDay = require('./ScheduleDay');

class Schedule {

    constructor(json) {
        this.potId = null;
        this.days = [];
        this.startedDate = null;
        this.maxTemperature = null;
        this.minTemperature = null;
        if (json) {
            if (!json.days)
                throw new Error('Schedule json: invalid format on days');
            this.potId = json.potId;
            this.startedDate = json.startedDate;
            this.maxTemperature = json.maxTemperature;
            this.minTemperature = json.minTemperature;
            if (!Array.isArray(json.days)) {
                if (json.days instanceof Object) {
                    let array = [];
                    Object.keys(json.days).forEach(key => {
                        array.push(new ScheduleDay(json.days[key]));
                    });
                    this.days = array;
                } else {
                    throw new Error('Schedule json: invalid format for days');
                }
            }
            else {
                let array = [];
                json.days.forEach(day => array.push(new ScheduleDay(day)));
                this.days = array;
            }
        }
    }

    addDay(day) {
        this.days.push(day);
        day.id = this.days.length - 1;
    }

}

module.exports = Schedule;