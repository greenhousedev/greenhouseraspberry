let ParametersConfiguration = require('./ParametersConfiguration');

class ScheduleDay {

    constructor(json) {
        this.id = null;
        this.dayIndex = null;
        this.parametersConfigurationEntries = [];
        if (json) {
            if (json.id === undefined)
                console.log('ScheduleDay json: invalid format on id');
                // throw new Error('ScheduleDay json: invalid format on id');

            if (!json.dayIndex)
                throw new Error('ScheduleDay json: invalid format dayIndex');

            this.id = json.id;
            this.dayIndex = parseInt(json.dayIndex);
            if (json.parametersConfigurationEntries) {
                if (Array.isArray(json.parametersConfigurationEntries)) {
                    let array = [];
                    json.parametersConfigurationEntries.forEach(ds => array.push(new ParametersConfiguration(ds)));
                    this.parametersConfigurationEntries = array;
                } else if (json.parametersConfigurationEntries instanceof Object) {
                    let array = [];
                    Object.keys(json.parametersConfigurationEntries).forEach(key => {
                        array.push(new ParametersConfiguration(json.parametersConfigurationEntries[key]));
                    });
                    this.parametersConfigurationEntries = array;
                }
            }
        }
    }

    addParametersConfigurationEntry(parametersConfiguration) {
        this.parametersConfigurationEntries.push(parametersConfiguration);
        parametersConfiguration.id = this.parametersConfigurationEntries.length - 1;
    }
}

module.exports = ScheduleDay;