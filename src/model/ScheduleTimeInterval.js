class ScheduleTimeInterval {

    constructor(json) {
        this._hour = '00';
        this._minutes = '00';
        if (json) {
            if ((json.hour === undefined || json.minutes === undefined) &&
                (json._hour === undefined || json._minutes === undefined)) {
                throw new Error('ScheduleTimeInterval json: invalid format: ' + JSON.stringify(json));
            }
            this.hour = json.hour || json._hour;
            this.minutes = json.minutes || json._minutes;
        }
    }

    get minutes() {
        return this._minutes;
    }

    set minutes(value) {
        this._minutes = this.formatNumber(value);
    }

    get hour() {
        return this._hour;
    }

    set hour(value) {
        this._hour = this.formatNumber(value);
    }

    formatNumber(number) {
        return parseInt(number);
    }

}

module.exports = ScheduleTimeInterval;