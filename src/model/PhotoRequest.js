class PhotoRequest {

   /* requestedAt: Date;
    processing: boolean;
    completed: boolean;*/

    constructor(json) {
        this.processing = false;
        this.completed = false;
        this.success = false;
        this.requested = false;
        this.scheduleParamsId = null;
        this.formattedStartTime = 0;
        if(json) {
            Object.assign(this, json);
        }
    }

    complete(success) {
        this.processing = false;
        this.completed = true;
        this.success = success;
    }

    setProcessing() {
        this.processing = true;
    }
}

module.exports = PhotoRequest;
