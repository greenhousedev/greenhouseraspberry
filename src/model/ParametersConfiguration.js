let ScheduleTimeInterval = require('./ScheduleTimeInterval');

class ParametersConfiguration {

    constructor(json) {
        this.id = null;
        this.light = 0;
        this.pump = 0;
        this.fan = 0;
        this.photo = 0;
        this.temperature = 0;
        this.startTime = new ScheduleTimeInterval();
        this.endTime = new ScheduleTimeInterval();

        if (json) {
            if (json.id === undefined) {
                // throw new Error('DaySchedule json: invalid format on id');
                console.log('DaySchedule json: invalid format on id');
            }
            if (json.light === undefined) {
                throw new Error('DaySchedule json: invalid format on light');
            }
            /*if (json.temperature === undefined) {
                throw new Error('DaySchedule json: invalid format on temperature');
            }*/
            if (json.pump === undefined) {
                throw new Error('DaySchedule json: invalid format on pump');
            }
            if (json.startTime === undefined) {
                throw new Error('DaySchedule json: invalid format on startTime');
            }
            /*if (json.endTime === undefined) {
                throw new Error('DaySchedule json: invalid format endTime');
            }*/

            /*this.id = json.id;
            this.light = json.light;
            this.pump = json.pump;
            this.fan = json.fan;
            this.temperature = json.temperature;*/

            Object.assign(this, json);

            this.startTime = new ScheduleTimeInterval(json.startTime);
            this.endTime = new ScheduleTimeInterval(json.endTime);
        }
    }
}

module.exports = ParametersConfiguration;
