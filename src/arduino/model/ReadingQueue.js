const {Subject} = require('rxjs');

class ReadingQueue {

	constructor(action) {
		if (!action) {
			throw new Error('Undefined action on new ReadingQueue');
		}
		this.action = action;
		this.subject = new Subject();

		this.interval = setTimeout(() => {
			this.subject.error(`Arduino Not responding on action ${action}. Be sure Arduino is plugged in or the sensor is wired correctly.`);
		}, 2990);

	}
}

module.exports = ReadingQueue;

