class ArduinoResponse {

	constructor(json){
		this.action = '';
		this.error = '';
		this.light = '';
		this.ph = '';

		if(json){
			try {
				const object = JSON.parse(json);
				this.action = object.action;
				this.error = object.error;
				this.light = object.light;
				this.ph = object.ph;
			}catch (e){
				console.log(e);
			}
		}

	}

}

module.exports = ArduinoResponse;