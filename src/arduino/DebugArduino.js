const SerialPort = require('serialport');
const Parser = require('@serialport/parser-readline');

let port;

port = new SerialPort('/dev/ttyUSB0', {baudRate: 9600});
const parser = new Parser();
port.pipe(parser);

parser.on('data', line => {
	if (line) {
		console.log(line);
		let lastResponse = JSON.parse(line);
		console.log(lastResponse)
	}else{
		console.log('empty response');
	}
});
const delimiter = '\n';

// console.log(port);

setInterval(()=>{
	port.write('{"action":"READ_ALL_SENSORS"}'+delimiter);
},1000)
