const SerialPort = require('serialport');
const Parser = require('@serialport/parser-readline');
const ReadingQueue = require('./model/ReadingQueue');
const ArduinoResponse = require('./model/ArduinoResponse');

let port;
let readingCache = {};
let lastRegisterPromise;

/**
 * @private
 * @return {Promise<any>}
 */
const registerArduino = () => {
	if (!lastRegisterPromise) {
		lastRegisterPromise = new Promise((resolve, reject) => {
			SerialPort.list()
				.then(ports => {
					let found = false;
					ports.forEach((p) => {
						// console.log('Found serial ports: ' + p.path);
						if (p.path === '/dev/ttyUSB0') {
							found = true;
							port = new SerialPort('/dev/ttyUSB0', {baudRate: 9600});
							const parser = new Parser();
							port.pipe(parser);
							parser.on('data', onDataReceived);
						}
					});
					if (!found) {
						lastRegisterPromise = null;
						reject('Not found /dev/ttyUSB0');
					} else {
						lastRegisterPromise = null;
						resolve(port);
					}
				})
				.catch(err => {
						lastRegisterPromise = null;
						console.error(err);
						reject(err);
					}
				);
		});
	}
	return lastRegisterPromise;
};

/**
 * @private
 * @param line
 */
const onDataReceived = line => {
	if (line) {
		let lastResponse = new ArduinoResponse(line);
		const readingQueue = getReadingQueue(lastResponse.action);
		if (readingQueue) {
			const {subject, interval} = readingQueue;
			if (!lastResponse.error) {
				subject.next(lastResponse);
				subject.complete(lastResponse);
				clearTimeout(interval);
				delete deleteReadingQueue(lastResponse.action);
			}
			else {
				subject.error(lastResponse.error);
				subject.complete(lastResponse);
				clearTimeout(interval);
				delete deleteReadingQueue(lastResponse.action);
			}
		}

	} else {
		// Object.keys(readingCache).forEach(key => {
		// 	readingCache[key].subject.error('Arduino Not responding. Be sure Arduino is plugged in.');
		// 	readingCache[key].subject.complete();
		// })
	}
};

/**
 * @param action
 * @return {string}
 */
const getJsonStringForAction = (action) => {
	return JSON.stringify({
		action
	})
};

/**
 * @return {Promise<*>}
 */
const verifyPort = async () => {
	if (port) {
		return port;
	} else {
		return await registerArduino()
	}
};

/**
 * @param action
 * @return ReadingQueue
 */
const getReadingQueue = (action) => {
	if (readingCache && action) {
		return readingCache[action]
	}
};

const deleteReadingQueue = (action) => {
	if (action && readingCache[action])
		delete readingCache[action];
};

const addReadingQueue = (action) => {
	if (action) {
		readingCache[action] = new ReadingQueue(action);
		return readingCache[action];
	}
}

/**
 * @param action
 * @return {Promise<any>}
 */
const sendAction = async (action) => {
	if (action) {
		try {
			const localPort = await verifyPort();
			const delimiter = '\n';
			let readingQueue = getReadingQueue(action);
			if (!readingQueue || (readingQueue && readingQueue.subject.hasError)) {
				deleteReadingQueue(action);
				const message = getJsonStringForAction(action) + delimiter;
				readingQueue = addReadingQueue(action);
				localPort.write(message);
			}
			return readingQueue.subject.asObservable().toPromise();
		} catch (e) {
			console.log(e);
			throw new Error('Undefined Arduino Serial Port. Be sure Arduino is plugged in.');
		}
	} else {
		throw new Error('Undefined arduino reading action');
	}
};


if (!port) {
	registerArduino().catch(err => {
		console.log(err);
		console.log('Undefined Arduino Serial Port. Be sure Arduino is plugged in.');
	});
}

module.exports = {
	sendAction
};