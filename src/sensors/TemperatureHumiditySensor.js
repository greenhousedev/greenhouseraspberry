const nodeDhtSensor = require('node-dht-sensor');
const {TEMPERATURE_AND_HUMIDITY} = require('../utils/PinsConfig');

/**
 * @return {Promise<{humidity: number,temperature: number,isValid: boolean,errors: number}>}
 */
async function read() {
	return await nodeDhtSensor.read(22, TEMPERATURE_AND_HUMIDITY);
}

module.exports = {
	read
};