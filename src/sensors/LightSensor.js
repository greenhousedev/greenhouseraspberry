const {log} = require('../utils/Log');
const {spawn} = require('child_process');
const {sendAction} = require('../arduino/ArduinoBridge');
const ARDUINO_LIGHT_SENSOR_KEY = 'READ_LIGHT_SENSOR_KEY';

/**
 * @return {Promise<{lux: number,integration_time: number,full: number,ir: number,gain: number}>}
 */
function readPython() {
	return new Promise((resolve, reject) => {
		const process = spawn('python', [__dirname + '/../../scripts/python/readLight.py']);
		let result;
		process.stdout.on('data', (data) => {
			result = JSON.parse(data);
		});
		process.stderr.on('data', (data) => {
			resolve({lux: null, integration_time: null, full: null, ir: null, gain: null});
		});
		process.on('close', (code) => {
			resolve(result)
		});
	})
}

const read = async () => {
	try {
		const {action, light} = await sendAction(ARDUINO_LIGHT_SENSOR_KEY);
		return light;
	} catch (e) {
		log(e.message || e);
		return 0;
	}
};


module.exports = {
	read
};


