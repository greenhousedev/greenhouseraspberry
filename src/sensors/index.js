const TemperatureHumiditySensor = require('./TemperatureHumiditySensor');
const ARDUINO_ALL_SENSORS_KEY = 'READ_ALL_SENSORS';
const {sendAction} = require('../arduino/ArduinoBridge');

/**
 * @return {Promise<{temperature: number, humidity: number, ph, light: any}>}
 */
async function readSensors() {
	let {ph, light} = await sendAction(ARDUINO_ALL_SENSORS_KEY);
	let {temperature, humidity} = await TemperatureHumiditySensor.read();

	ph = 7 + (ph % 1);

	temperature = temperature !== undefined && temperature !== null ? temperature.toFixed(2) : null;
	humidity = humidity !== undefined && humidity !== null ? humidity.toFixed(2) : null;
	ph = ph !== undefined && ph !== null ? ph.toFixed(2) : null;
	light = light !== undefined && light !== null ? light.toFixed(2) : null;

	return {temperature, humidity, ph, light};
}

module.exports = {
	readSensors
};