const {sendAction} = require('../arduino/ArduinoBridge');
const {log} = require('../utils/Log');

const ARDUINO_PH_SENSOR_READ_KEY = 'READ_PH_SENSOR';

const read = async () => {
	try {
		let {ph} = await sendAction(ARDUINO_PH_SENSOR_READ_KEY);
		ph = 7 + (ph % 1);
		return {ph};
	} catch (e) {
		log(e.message || e);
		return {ph: null};
	}
};

module.exports = {
	read
};


