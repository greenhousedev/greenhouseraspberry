const moment = require('moment');
const firebase = require('./utils/FirebaseConfig');
const {Gpio} = require('onoff');
const {POT_ID} = require('../pot.config.json');
const {Observable} = require('rxjs');
const {log} = require('./utils/Log');
const dns = require('dns');
const Schedule = require('./model/Schedule');
const PhotoRequest = require('./model/PhotoRequest');
const CameraUtils = require('./utils/CameraUtils');
const StorageUtils = require('./utils/StorageUtils');
const {LIGHT_RELAY, PUMP_RELAY, FAN_RELAY} = require('./utils/PinsConfig');
const {readSensors} = require('./sensors');
const {startWebServer} = require('./utils/WebServer');
const {sendMessage, SCHEDULE_MAX_TEMPERATURE, SCHEDULE_MIN_TEMPERATURE, SCHEDULE_PHOTO_REQUEST} = require('./utils/NotificationsUtils');
const GLOBAL_POT_ROUTE = `/pots/${POT_ID}`;
const io = require('./utils/WebSocketServer');
const {startBrowser} = require('./utils/BrowserUtils');

class RaspberryApplication {

	constructor() {
		startBrowser();
		startWebServer().catch(console.log);
		// .then(()=>{
		// }).catch(console.log);

		this.userId = null;

		//define relays pins
		this.light = new Gpio(LIGHT_RELAY, 'out');
		this.pump = new Gpio(PUMP_RELAY, 'out');
		this.fan = new Gpio(FAN_RELAY, 'out');

		this.readSensorsInterval = null;
		this.updateTimestampInterval = null;
		this.lastRetryTimeout = null;

		this.activeSafetyCheck = null;

		this.activeSchedule = null;

	}

	init() {
		if (!POT_ID)
			return log('Undefined PotId');
		else if (POT_ID === 'unset')
			return log('Please set the pot id in pot.config.json');

		this.pump.writeSync(1);
		this.fan.writeSync(1);
		this.light.writeSync(1);

		this.checkInternetConnection()
			.subscribe(() => {
				this.startProgram();
			});

	}

	/**
	 * @private
	 */
	checkInternetConnection() {
		return new Observable((subscriber) => {
			let interval = setInterval(() => {
				dns.resolve('www.google.com', (err) => {
					if (err) {
						io.emit('no_internet');
						log("No Internet connection. Retry in 10 seconds.");
					} else {
						io.emit('internet_connected');
						log("Connected to internet");
						subscriber.next();
						subscriber.complete();
						clearInterval(interval);
						interval = null;
					}
				});
			}, 1000 * 10);
		});
	}

	/**
	 * @private
	 */
	startProgram() {
		log('Read pot data');
		firebase.database()
			.ref('/pots/' + POT_ID)
			.once('value')
			.then((snapshot) => {
				let val = snapshot.val();
				if (val.userId) {
					this.userId = val.userId;
					log('The pot is assigned to the user ' + val.userId);
					this.monitoring();
				} else {
					log('The pot is not assigned to the user');
					this.waitForActivation();
				}
			});
	}

	/**
	 * @private
	 * @param userId
	 */
	monitoring() {
		log('Start Monitoring');
		const USER_ROUTE = `/users/${this.userId}`;
		const POT_ROUTE = `/users/${this.userId}/pots/${POT_ID}`;

		firebase.database().ref(USER_ROUTE)
			.once('value')
			.then((snapshot) => {
				let user = snapshot.val();
				if (user && user.state.toLowerCase() === 'active') {
					log('User is valid and active');

					firebase.database()
						.ref(GLOBAL_POT_ROUTE + '/activations')
						.push({
							date: moment(new Date()).unix(),
							userId: this.userId
						});

					firebase.database()
						.ref(POT_ROUTE)
						.update({id: POT_ID, relayState: {fan: 1, pump: 1, light: 1}});

					firebase.database()
						.ref(POT_ROUTE + '/schedule')
						.on('value', (snapshot) => {
							let val = snapshot.val();
							if (val && val.startedDate) {
								log('Valid schedule found');
								this.startSchedule(val);
							} else {
								log('No schedule found');
							}
						});

					// listen photo request
					firebase.database()
						.ref(POT_ROUTE + '/photoRequest')
						.on('value', snapshot => this.handlePhotoRequest(snapshot.val()));

					// listen controller changes
					firebase.database()
						.ref(POT_ROUTE + '/relayState')
						.on('child_changed', (snapshot) => {
							let val = snapshot.val();
							if (snapshot.key === 'pump' && (val == 0 || val == 1)) {
								this.pump.writeSync(val);
							}
							if (snapshot.key === 'fan' && (val == 0 || val == 1)) {
								this.fan.writeSync(val);
							}
							if (snapshot.key === 'light' && (val == 0 || val == 1)) {
								this.light.writeSync(val);
							}
						});

					this.startReadSensorsLoop();
					this.startLastOnlineLoop();


				} else {
					log('User is invalid or not found', new Date());
					firebase.database()
						.ref(GLOBAL_POT_ROUTE + '/errors')
						.push({
							date: moment(new Date()).unix(),
							userId: this.userId,
							message: 'Unsatisfactory setup attempt',
							user
						});
				}
			})
			.catch(console.log);
	}

	/**
	 * @private
	 * @param val
	 */
	handlePhotoRequest(val) {
		return new Promise((resolve, reject) => {
			const POT_ROUTE = `/users/${this.userId}/pots/${POT_ID}`;

			if (!val) return;
			let request = new PhotoRequest(val);
			if (request.completed) return;
			if (request.processing) return;

			log('Sew photo request:');

			request.setProcessing();
			firebase.database().ref(POT_ROUTE + '/photoRequest').set(request);

			CameraUtils.takePhoto()
				.then(path => StorageUtils.uploadPotScreenshot(path, POT_ID))
				.then(downloadUrl => {
					request.complete(true);
					firebase.database().ref(POT_ROUTE + '/photoRequest').set(request);
					firebase.database().ref(POT_ROUTE).update({screenshotPath: downloadUrl});
					log("Take photo SUCEEDED");
					resolve();
				})
				.catch(error => {
					request.complete(false);
					firebase.database().ref(POT_ROUTE + '/photoRequest').set(request);
					log("Take photo ERROR: ", error);
					reject()
				})
		})
	}

	/**
	 * @private
	 */
	startReadSensorsLoop() {
		const POT_ROUTE = `/users/${this.userId}/pots/${POT_ID}`;
		this.readSensorsInterval = setInterval(async () => {
			try {
				const response = await readSensors();
				if (this.activeSchedule) {
					this.checkScheduleSafetyParams(this.activeSchedule, response)
						.then(safety => {
							if (safety) {
								this.activeSafetyCheck = true;
								firebase.database()
									.ref(POT_ROUTE + '/relayState')
									.update(safety);
							}
							else {
								if (this.activeSafetyCheck) {
									this.activeSafetyCheck = false;
									this.retryScheduleVerification(this.activeSchedule, 1, 'Back to safety check');
								}
							}
						}).catch(console.log);
				}
				io.emit('sensors', JSON.stringify(response));
				firebase
					.database()
					.ref(POT_ROUTE + '/sensors')
					.set(response);
			} catch (e) {
				console.log(e)
			}
		}, 3000);
	}

	/**
	 * @private
	 */
	startLastOnlineLoop() {
		const POT_ROUTE = `/pots/${POT_ID}`;
		const USER_POT_ROUTE = `/users/${this.userId}/pots/${POT_ID}`;

		let lastOnlinePot = firebase.database().ref(POT_ROUTE + '/lastOnline/timestamp');
		let lastOnline = firebase.database().ref(USER_POT_ROUTE + '/lastOnline/timestamp');

		lastOnlinePot.set(firebase.database.ServerValue.TIMESTAMP);
		lastOnline.set(firebase.database.ServerValue.TIMESTAMP);
		this.updateTimestampInterval = setInterval(() => {
			lastOnlinePot.set(firebase.database.ServerValue.TIMESTAMP);
			lastOnline.set(firebase.database.ServerValue.TIMESTAMP);
		}, 30 * 1000)
	}

	/**
	 * @private
	 */
	waitForActivation() {
		log('Waiting for validation');
		let ref = firebase.database().ref(GLOBAL_POT_ROUTE);
		ref.on('child_changed', (snapshot) => {
			let val = snapshot.val();
			if (snapshot.key === 'userId' && val) {
				log('The pot was assigned', new Date());
				this.init();
			} else if (snapshot.key === 'used') {
				firebase.database()
					.ref('/pots/' + POT_ID)
					.once('value')
					.then((snapshot) => {
						let {used, userId} = snapshot.val();
						if (!used && !userId) {
							this.deactivate(val.userId);
							log('Waiting for validation');
						}
					});
			}
		});
	}

	/**
	 * @private
	 * @param scheduleJson
	 */
	async startSchedule(scheduleJson) {
		log('Starting schedule', new Date());
		const POT_ROUTE = `/users/${this.userId}/pots/${POT_ID}`;
		try {
			let schedule = new Schedule(scheduleJson);
			this.activeSchedule = schedule;
			let currentDate = moment();
			let startDate = moment.unix(schedule.startedDate);
			startDate.hour(0);
			startDate.minute(0);
			let pastDays = Math.trunc(moment.duration(currentDate.diff(startDate)).asDays()) + 1;
			let foundDay = schedule.days.find(day => day.dayIndex === pastDays);
			if (foundDay) {
				log(` [SCHEDULE] Found Active Day`);
				let formattedCurrentTime = this.formattedDate(currentDate.hour(), currentDate.minute());
				console.log("Schedule foundDay=" + JSON.stringify(foundDay) + ", formattedCurrentTime=" + formattedCurrentTime);

				// find the current param config; the latest start time smaller than current date
				let parametersConfigStartTime = 0;
				let parametersConfig = null;
				foundDay.parametersConfigurationEntries.forEach(p => {
					let formattedStartTime = this.formattedDate(p.startTime.hour, p.startTime.minutes);
					console.log("Schedule looking parametersConfig " + JSON.stringify(p));
					if (parametersConfigStartTime < formattedStartTime && formattedStartTime <= formattedCurrentTime) {
						parametersConfigStartTime = formattedStartTime;
						parametersConfig = p;
					}
				});
				console.log("Schedule parametersConfig=" + JSON.stringify(parametersConfig));

				//calculate next param config
				let nextParametersConfig = null;
				let earliestStartTime = 0;
				foundDay.parametersConfigurationEntries.forEach(p => {
					let formattedStartTime = this.formattedDate(p.startTime.hour, p.startTime.minutes);
					if ((formattedStartTime < earliestStartTime || earliestStartTime === 0)
						&& formattedStartTime > parametersConfigStartTime && formattedCurrentTime < formattedStartTime) {
						earliestStartTime = formattedStartTime;
						nextParametersConfig = p;
					}
				});
				console.log("Schedule nexParametersConfig=" + JSON.stringify(nextParametersConfig));

				let light, pump, fan;

				if (parametersConfig) {
					log(` [SCHEDULE] Configurations`);

					light = parametersConfig.light || 0;
					if (light !== 0 && light !== 1) light = 0;
					this.light.writeSync(light);

					pump = parametersConfig.pump || 0;
					if (pump !== 0 && pump !== 1) pump = 0;
					this.pump.writeSync(pump);

					fan = parametersConfig.fan || 0;
					if (fan !== 0 && fan !== 1) fan = 0;
					this.fan.writeSync(fan);

					// checking if we should add a photo request
					if (parametersConfig.photo === 1) {
						log(` [SCHEDULE] Photo Request`);
						firebase.database()
							.ref(POT_ROUTE + '/photoRequest')
							.once('value')
							.then((snapshot) => {

								let val = snapshot.val();
								if (!val) return;

								let formattedStartTime = this.formattedDate(parametersConfig.startTime._hour, parametersConfig.startTime._minutes);
								let request = new PhotoRequest(val);
								console.log("Schedule current photo request: " + JSON.stringify(request) + ", formattedStartTime=" + formattedStartTime);

								// check if we already requested photo for this param time
								if (request.formattedStartTime && request.formattedStartTime === formattedStartTime) return;

								// a request is active at this moment, will ignore this one.
								if (request.completed === false) return;

								let newRequest = new PhotoRequest();
								newRequest.requested = true;
								newRequest.formattedStartTime = formattedStartTime;

								log("Schedule adding photo request");

								this.handlePhotoRequest(newRequest).then(() => {
									log("Schedule adding photo request");
									sendMessage(SCHEDULE_PHOTO_REQUEST, this.userId, 'Culture Update', `Pot with id${POT_ID} updated the photo culture`).catch(e => log(e));
								}).catch(console.log);
							});
					}

					console.log("Schedule relay pump=" + pump + ", light=" + light + ", fan=" + fan);

				}

				let updateObject = {light, pump, fan};
				const safety = await this.checkScheduleSafetyParams(schedule);

				if (safety) {
					updateObject = {...updateObject, ...safety};
				}
				firebase.database()
					.ref(POT_ROUTE + '/relayState')
					.update(updateObject);

				let millis = 60 * 60 * 1000;

				if (nextParametersConfig) {
					let finish = moment();
					finish.hour(nextParametersConfig.startTime.hour);
					finish.minutes(nextParametersConfig.startTime.minutes);
					millis = Math.trunc(moment.duration(finish.diff(currentDate)).asMilliseconds());
				}

				log("Schedule next schedule duration = " + Math.trunc(millis / (1000 * 60)) + ' minutes');

				this.retryScheduleVerification(schedule, millis, 'waiting');
			}
			else {
				this.retryScheduleVerification(schedule, 60 * 1000, 'monitoring');
			}
		} catch (e) {
			log(" [FROM SCHEDULE] "+e.toString());
		}
	}

	/**
	 * @private
	 */
	async checkScheduleSafetyParams(schedule, sensorsValues) {


		if (schedule && schedule.maxTemperature || schedule.minTemperature) {
			const returnObject = {};
			if (!sensorsValues) {
				sensorsValues = await readSensors();
			}
			const temperature = sensorsValues.temperature;

			if (schedule.maxTemperature && temperature > schedule.maxTemperature) {
				sendMessage(SCHEDULE_MAX_TEMPERATURE, this.userId, 'Warning', `Pot with id${POT_ID} has reached the maximum temperature provided in the schedule`).catch(e => log(e));
				log('Warning: Fan and light forced by the Schedule Max temperature.');
				returnObject.fan = 0;
				returnObject.light = 1;
				this.fan.writeSync(0);
				this.light.writeSync(1);
			} else if (schedule.minTemperature && temperature < schedule.minTemperature) {
				sendMessage(SCHEDULE_MIN_TEMPERATURE, this.userId, 'Warning', `Pot with id: ${POT_ID} has reached the minimum temperature provided in the schedule`).catch(e => log(e));
				log('Warning: Fan and light forced by the Schedule Min temperature.');
				returnObject.fan = 1;
				returnObject.light = 0;
				this.fan.writeSync(1);
				this.light.writeSync(0);
			}

			return Object.keys(returnObject).length ? returnObject : false;
		} else {
			return false;
		}
	}

	/**
	 * @private
	 * @param schedule
	 * @param time
	 * @param logState
	 */
	retryScheduleVerification(schedule, time, logState) {
		if (this.lastRetryTimeout) {
			clearTimeout(this.lastRetryTimeout);
			this.lastRetryTimeout = null;
		}
		this.lastRetryTimeout = setTimeout(() => {
			log(logState);
			this.startSchedule(schedule);
		}, time)
	}

	/**
	 * @private
	 */
	deactivate() {
		log('Clear all readings', new Date());
		if (this.lastRetryTimeout) {
			clearTimeout(this.lastRetryTimeout);
			this.lastRetryTimeout = null;
		}
		if (this.readSensorsInterval) {
			clearInterval(this.readSensorsInterval);
			this.readSensorsInterval = null;
		}
		if (this.updateTimestampInterval) {
			clearInterval(this.updateTimestampInterval);
			this.updateTimestampInterval = null;
		}
	}

	/**
	 * @private
	 * @param hour
	 * @param minutes
	 * @return {*}
	 */
	formattedDate(hour, minutes) {
		return hour * 100 + minutes;
	}

}


module.exports = RaspberryApplication;


