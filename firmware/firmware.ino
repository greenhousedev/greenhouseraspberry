#include <Wire.h> 
#include <Arduino_JSON.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_TSL2591.h"

// Example for demonstrating the TSL2591 library - public domain!

// connect SCL to I2C Clock
// connect SDA to I2C Data
// connect Vin to 3.3-5V DC
// connect GROUND to common ground

Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591); // pass in a number for the sensor identifier (for your use later)

String inputString = "";         
boolean stringComplete = false;  

const int analogInPin = A0;

const String READ_ALL_SENSOR_SENSORS = "READ_ALL_SENSORS";
const String READ_PH_SENSOR_KEY = "READ_PH_SENSOR";
const String READ_LIGHT_SENSOR_KEY = "READ_LIGHT_SENSOR_KEY";

unsigned long int avgValue;
int sensorValue = 0; 
float b;
int buf[10],temp;
boolean light_sensor_init = false;

void setup() {
  Serial.begin(9600);
  inputString.reserve(200);
  if(tsl.begin()){;
     light_sensor_init = true;
     configureSensor();
  }
}

void loop() {
  serialEvent(); 
  if (stringComplete) {
    JSONVar inputJson = JSON.parse(inputString);
    if (JSON.typeof(inputJson) == "undefined") {
        Serial.println(getErrorJSONString("UNDEFINED_ACTION","Parsing input failed! It may be the invalid JSON format."));
    }else{
        if(inputJson.hasOwnProperty("action")){ 
          if(inputJson["action"] == READ_ALL_SENSOR_SENSORS){
            Serial.println(getResponseJsonString(READ_ALL_SENSOR_SENSORS, readLightSensor(), readPhSensor()));
            } else if(inputJson["action"] == READ_PH_SENSOR_KEY) {
                Serial.println(getResponseJsonString(READ_PH_SENSOR_KEY, 0, readPhSensor()));
            } else if(inputJson["action"] == READ_LIGHT_SENSOR_KEY) {
              if(light_sensor_init){
                Serial.println(getResponseJsonString(READ_LIGHT_SENSOR_KEY, readLightSensor(), 0));
              }else{
                Serial.println(getErrorJSONString(READ_LIGHT_SENSOR_KEY, "Light Senor not strted."));
                }
            }else{
                Serial.println(getErrorJSONString(inputJson["action"], "Invalid action type."));
            }
        }
    }

    inputString = "";
    stringComplete = false;
    
  }
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      stringComplete = true;
    }else{
       inputString += inChar ;
    }
  }
}

String getErrorJSONString(JSONVar key, String message){
    JSONVar errorObject;
    errorObject["action"] = key;
    errorObject["error"] = message;
    return JSON.stringify(errorObject);
}

String getResponseJsonString(String key, float light, float ph){
    JSONVar responseObject;
 
    responseObject["action"] = key;
    responseObject["ph"] = ph;
    responseObject["light"] = light;
    
    return JSON.stringify(responseObject);
}


float readPhSensor(){
    for(int i=0;i<10;i++){
      buf[i]=analogRead(analogInPin);
      delay(10);
    }
    for(int i=0;i<9;i++){
      for(int j=i+1;j<10;j++){
        if(buf[i]>buf[j]){
          temp=buf[i];
          buf[i]=buf[j];
          buf[j]=temp;
        }
      }
    }
   avgValue=0;
   for(int i=2;i<8;i++){
    avgValue+=buf[i];
   }
   
   float pHVol=(float)avgValue*5.0/1024/6;
   float phValue = -5.70 * pHVol + 21.34;
  
   return phValue;
}


/**************************************************************************/
/*
    Configures the gain and integration time for the TSL2591
*/
/**************************************************************************/
void configureSensor(void)
{
  // You can change the gain on the fly, to adapt to brighter/dimmer light situations
  //tsl.setGain(TSL2591_GAIN_LOW);    // 1x gain (bright light)
  tsl.setGain(TSL2591_GAIN_MED);      // 25x gain
  //tsl.setGain(TSL2591_GAIN_HIGH);   // 428x gain
  
  // Changing the integration time gives you a longer time over which to sense light
  // longer timelines are slower, but are good in very low light situtations!
  //tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS);  // shortest integration time (bright light)
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS);
  tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS);  // longest integration time (dim light)

  /* Display the gain and integration time for reference sake */  

  tsl2591Gain_t gain = tsl.getGain();
  switch(gain)
  {
    case TSL2591_GAIN_LOW:
//      Serial.println(F("1x (Low)"));
      break;
    case TSL2591_GAIN_MED:
//      Serial.println(F("25x (Medium)"));
      break;
    case TSL2591_GAIN_HIGH:
//      Serial.println(F("428x (High)"));
      break;
    case TSL2591_GAIN_MAX:
//      Serial.println(F("9876x (Max)"));
      break;
  }

}

float readLightSensor()
{
  // More advanced data read example. Read 32 bits with top 16 bits IR, bottom 16 bits full spectrum
  // That way you can do whatever math and comparisons you want!
  uint32_t lum = tsl.getFullLuminosity();
  uint16_t ir, full;
  ir = lum >> 16;
  full = lum & 0xFFFF;
  return tsl.calculateLux(full, ir);
}



